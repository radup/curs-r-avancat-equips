# Curs R Avançat Equips

Curs d'R Avançat per a Equips de Treball, que necessiten aprendre a dominar control de versions del codi, formes de compartir el codi de forma més segura entre diferents persones i ordinadors. Es preveu aprendre a emprar git en local i gitlab remot, renv per a gestió de versions de paquets en projectes de desenvolupament en R. De la mateixa manera es mostraran difents maneres d'organitzar el codi i la documentació asociada, així com els paquets que eventualment es facin. I es té previst aprendre a crear dashboards per a aplicacions dinàmiques de diverses formes. 

## Apunts

Els apunts del curs són aquí:

https://seeds4c.org/CursRAvancatEquips

## Afegeix els teus arxius

- [ ] [Crea](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) o [puja](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) arxius
- [ ] [Afegeix arxius a través de la linia de comandes](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) o envia via **push** un repositori Git existent amb la instrucció següent:

```
cd existing_repo
git remote add origin https://gitlab.com/radup/curs-r-avancat-equips.git
git branch -M main
git push -uf origin main
```

## Col·labora amb el teu equip

- [ ] [Convida a membres de l'equip i col·laboradors](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Crea una nova petició de fusió - merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)


***

## Llicència
Mentre no s'indiqui el contrari, el codi font d'aquest repositori es publica amb llicència GNU/GPL. I la documentació amb CC-BY-SA.
