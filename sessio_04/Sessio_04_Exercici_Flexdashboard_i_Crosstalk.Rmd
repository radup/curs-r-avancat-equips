---
title: "Dashboard dels Salons de Joc (amb flexdashboard i crosstalk)"
output: 
  flexdashboard::flex_dashboard:
    orientation: rows
    vertical_layout: fill
    social: menu
    source_code: embed
    self_contained: FALSE
    theme: flatly
---


```{r setup, include=FALSE}
# Html widgets
library(flexdashboard)
library(leaflet)
library(plotly)
library(crosstalk)
if (!require("summarywidget")) {renv::install("kent37/summarywidget")}; library(summarywidget)
library(DT)
if (!require("d3scatter")) {renv::install("jcheng5/d3scatter")}; library(d3scatter)

# Spatial data
library(sf)
library(raster)
library(dplyr)

# Visualisation and extras
library(viridis)
library(leafem)
library(leaflet.providers)

knitr::opts_chunk$set(cache = TRUE)
```

Data {data-orientation=columns data-icon="fa-table"}
===================================== 

```{r}
# Importa les dades
r1.ll <- rio::import(file.path("..", "sessio_02", "salons_de_joc_en_la_web.xlsx"))

r1_sf <- st_as_sf(r1.ll, coords = c("longitud", "latitud"), crs = 4326, agr = "constant")

# Definim el Shared Data Crosstalk object (sdc)
sdc <- SharedData$new(r1_sf)

```

Column {data-width=400}
------------------------------------

### Filters {data-height=170}

```{r}
bscols(
    filter_slider("ordre", "ordre", sdc, column=~ordre, step=1),
    filter_slider("cp", "cp_num", sdc, column=~cp_num, step=10)
)
```

Dades: **`r summarywidget(sdc, statistic = 'count')`**, Mitjana Ordre = **`r summarywidget(sdc, statistic = 'mean', column = 'ordre', digits = 1)`**, Codi Postal Mig = **`r summarywidget(sdc, statistic = 'mean', column = 'cp_num', digits = 0)`%**. 

### Taula de dades {data-height=600}

```{r width='100%'}
DT::datatable(sdc, filter = "top",
              height='100%', width='100%', 
              rownames = FALSE,
              extensions = c('Buttons', 'Scroller'),
              style="bootstrap", class="compact",
              options = list(dom = 'Blrtip',
                             deferRender=TRUE, 
                             scrolly=80,
                             scroller=TRUE,
                             columnDefs = list(
                               list(
                                 visible = FALSE, targets = c(0:1, 3, 6:9, 11:12) # Aquí estic 
                                 )
                               ),
                             buttons = c(I('colvis'),  # turn columns on and off
                                         'copy', # Copy to clipboard
                                         'csv', # Download as csv
                                         'excel'))) # Download as excel
```


Column {data-width=300}
------------------------------------

### Boxplots {data-height=170 .box-lightgreen}

```{r}
p1 <- plot_ly(sdc, x = ~ordre, color = I("#209f88")) %>% 
  add_boxplot(name = "ordre") %>%
  layout(xaxis = list(zeroline=FALSE), ticks="", ticklen=0)
p2 <- plot_ly(sdc, x = ~`cp_num`, color = I("#209f88")) %>% 
  add_boxplot(name = "Codi Postal")  %>%
  layout(xaxis = list(zeroline=FALSE), ticks="", ticklen=0)
s <- subplot(p1, p2, nrows=2, margin=0.04) %>%
  highlight(on="plotly_selected") %>%
  layout(showlegend = FALSE, margin = list(l=0, r=0, t=0, b=0),
         ticks="", ticklen=0)
s
```


### Mapa ineractiu {data-height=600}

```{r}
bm <- leaflet(sdc, width="100%", height="100%", ) %>%
  
  # OS map layer
  addProviderTiles(providers$Esri.WorldImagery, group="ESRI Satellite",
                 options=leafletOptions(maxNativeZoom=19,maxZoom=100)) %>%
  addProviderTiles("OpenStreetMap",
           options=leafletOptions(maxNativeZoom=19,maxZoom=100)) %>%

  # Sample points
  addCircleMarkers(data=sdc, radius=3, weight=2, color="red") %>%
  
  # Add layer control elements
  addLayersControl(baseGroups = c("OpenStreetMap", "ESRI Satellite"),
                   options = layersControlOptions(collapsed = TRUE,
                                                  autoZIndex = F))

bm
```

```{r, message=F, echo=F}
# library(mapSpain)
# library(leaflet)
# 
# stations <- esp_get_railway(spatialtype = "point", epsg = 4326)
# 
# m <- leaflet(stations,
#   elementId = "railway",
#   width = "100%",
#   height = "60vh"
# ) %>%
#   addProviderEspTiles("IGNBase.Gris", group = "Base") %>%
#   addProviderEspTiles("MTN", group = "MTN") %>%
#   addProviderEspTiles("RedTransporte.Ferroviario",
#     group = "Lineas Ferroviarias"
#   ) %>%
#   addMarkers(
#     group = "Estaciones",
#     popup = sprintf(
#       "<strong>%s</strong>",
#       stations$rotulo
#     ) %>%
#       lapply(htmltools::HTML)
#   ) %>%
#   addLayersControl(
#     baseGroups = c("Base", "MTN"),
#     overlayGroups = c("Lineas Ferroviarias", "Estaciones"),
#     options = layersControlOptions(collapsed = FALSE)
#   )
# 
# m

```


Informació {.storyboard data-icon="fa-info-circle"}
==================================== 

### Bla bla...
