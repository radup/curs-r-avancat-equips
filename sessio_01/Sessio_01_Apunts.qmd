---
title: "Sessió 01"
subtitle: "Bones Pràctiques amb R en equips - git"
author: "Xavier de Pedro Puente, Ph.D."
format: 
  revealjs:
    logo: "quarto.png"
    footer: "Curs R Avançat per a Equips"
    slide-number: true
    show-slide-number: all
    menu:
      side: left
      width: normal
editor: visual
---

## Introducció (i): Dates

```         
1. Dilluns 4 de març - Bones Pràctiques amb R en equips - git
2. Dilluns 11 de març - Gestió de Projectes en R (1) amb git i renv
3. Dilluns 8 d’abril - Gestió de Projectes en R (2) Github i iniciar projecte per equips
4. Dilluns 22 d’abril - Gestió de Projectes en R (3) Avançar projecte i migrar a Gitlab Gencat
5. Dilluns 6 de maig - Creació d'Aplicacions Interactives (1) Quarto i R Markdown i (2) Shiny 
```

## Introducció (ii): Altres detalls

::: columns
::: {.column width="90%"}
Veurem diferents formes de generar contingut amb [Quarto markdown](https://quarto.org/docs/presentations/) al llarg de les sessions

::: nonincremental
```         
- html interactiu, 
    - amb revealjs (aquest document)
    - amb slidy (apunts d'avui sobre git)
- pdf,
- pptx,
- docx. 
```
:::
:::
:::

## Introducció (iii): Altres detalls

Veurem alguna aplicació interactiva

-   amb **Markdown** (html i javascript sense necessitat de servidor de R),

```         
    -   Exemple senzill: https://dades.ajuntament.barcelona.cat/consum-privat/index.Rmd
    -   Exemple avançat: https://ecodiv.earth/demos/demo_schijndelvoedselbos.html
```

-   i amb **Shiny** (requereix servidor de R i de Shiny), del tipus:

```         
    -   Exemple senzill: https://dades.ajuntament.barcelona.cat/estadistiques-cens-comercial/
    -   Exemple avançat: https://dades.ajuntament.barcelona.cat/la-ciutat-al-dia/
```

Aquesta presentació s'ha fet en html interactiu amb [revealjs](https://quarto.org/docs/presentations/revealjs)

## Introducció (iv): assistents i ordinadors

::: columns
::: {.column width="100%"}
::: incremental
-   personal funcionari d'administracions públiques, amb ordinadors portàtils amb MS Windows sense support de servidor linux amb Posit Workbench o Posit Server Opensource
-   sense permís d'administrador de l'ordinador
-   sense instal.lació dual amb Linux en paral.lel (Linkat o similar) ni opció fàcil corporativa de tenir sistema operatiu linux virtualitzat o "WSL" - Windows Subsystem for Linux. O sistema nix o sistemes basats en contenidors Docker, Pod o similars
:::
:::
:::

## Introducció (v): estat de l'art

-   RStudio local ("RStudio Desktop = Posit Desktop)
-   Posit Server FLOSS vs Posit Workbench ,
-   Posit Cloud (i altres clouds),
-   Núvol local de l'administració local (Gencat, Departament, Equip...)
-   R en equips d'escriptori + git contra servidor (local o remot)

## Introducció (vi): estat de l'art

-   En qualsevol cas:
    -   `git` per al control de canvis del codi personal i de l'equip, en el llenguatge de programació (o markup) que sigui
    -   paquet `renv` de R, per controlar les versions de les col·leccions de paquets de R en un projecte
    -   contenidors (docker, pod o similar) per controlar les versions de paquets de sistema.
-   Addendum extra: "[nix](https://nixos.org/)" per a casos molt avançats en entorns Linux.

## Manipulació de Dades amb tidyverse

-   Manipulació avançada de dades i bones pràctiques amb ~~data.table~~ data.frames (amb dtplyr sparklyr, o disk.frames si cal), tidyverse, enfocat a la gestió de grans bases de dades.
-   Organització de projectes: carpetes, funcions, scripts.
-   Lloc comú de treball.

```         
    -   scripts: unitat de xarxa local sense git vs unitat de xarxa local amb git local vs. git remot (per exemple, gitlab.com o gitlab gencat)
    -   funcions i `source()`
    -   paquets: Posit Package Manager vs, `drat` en unitat de xarxa local
```

## Apunts específics git

Obrir l'arxiu d'aquesta carpeta d'apunts:

-   Sessio_01_Compartir_via_git.Rmd \> clicar **Knit**

## 
